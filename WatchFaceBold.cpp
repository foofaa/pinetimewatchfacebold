#include "displayapp/screens/WatchFaceBold.h"
#include <cmath>
#include <lvgl/lvgl.h>
#include "displayapp/screens/BatteryIcon.h"
#include "displayapp/screens/BleIcon.h"
#include "displayapp/screens/Symbols.h"
#include "displayapp/screens/NotificationIcon.h"
#include "components/settings/Settings.h"

using namespace Pinetime::Applications::Screens;

WatchFaceBold::WatchFaceBold(Pinetime::Applications::DisplayApp* app,
                                 Controllers::DateTime& dateTimeController,
                                 Controllers::Battery& batteryController,
                                 Controllers::Ble& bleController,
                                 Controllers::NotificationManager& notificationManager,
                                 Controllers::Settings& settingsController)
  : Screen(app),
    currentDateTime {{}},
    dateTimeController {dateTimeController},
    batteryController {batteryController},
    bleController {bleController},
    notificationManager {notificationManager},
    settingsController {settingsController} {
  settingsController.SetClockFace(2);

  battery_colour = LV_COLOR_MAGENTA; // MAGENTA = debug colour (i.e. should never be seen, if colours are subsequently initialised correctly)
  lv_style_init(&style_battery);
  lv_style_set_line_color(&style_battery, LV_STATE_DEFAULT, battery_colour);
  lv_style_set_line_opa(&style_battery, LV_STATE_DEFAULT, LV_OPA_COVER);
  lv_style_set_line_width(&style_battery, LV_STATE_DEFAULT, 12);
  lv_style_set_line_rounded(&style_battery, LV_STATE_DEFAULT, true);

  lv_style_init(&style_digit_off);
  lv_style_set_line_color(&style_digit_off, LV_STATE_DEFAULT, LV_COLOR_BLACK);
  lv_style_set_line_opa(&style_digit_off, LV_STATE_DEFAULT, LV_OPA_TRANSP);
  lv_style_set_line_width(&style_digit_off, LV_STATE_DEFAULT, 10);
  lv_style_set_line_rounded(&style_digit_off, LV_STATE_DEFAULT, true);

  lv_style_init(&style_hour_on);
  lv_style_set_line_color(&style_hour_on, LV_STATE_DEFAULT, battery_colour);
  lv_style_set_line_opa(&style_hour_on, LV_STATE_DEFAULT, LV_OPA_COVER);
  lv_style_set_line_width(&style_hour_on, LV_STATE_DEFAULT, 10);
  lv_style_set_line_rounded(&style_hour_on, LV_STATE_DEFAULT, true);

  lv_style_init(&style_minute_on);
  lv_style_set_line_color(&style_minute_on, LV_STATE_DEFAULT, battery_colour);
  lv_style_set_line_opa(&style_minute_on, LV_STATE_DEFAULT, LV_OPA_COVER);
  lv_style_set_line_width(&style_minute_on, LV_STATE_DEFAULT, 16);
  lv_style_set_line_rounded(&style_minute_on, LV_STATE_DEFAULT, true);

  background = lv_obj_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_bg_color(background, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLACK);
  lv_obj_set_style_local_radius(background, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, 12);
  lv_obj_set_size(background, 240, 240);
  lv_obj_align(background, lv_scr_act(), LV_ALIGN_IN_TOP_LEFT, 0, 0);

  label_date_day = lv_label_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_text_font(label_date_day, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, &ocra_28);
  lv_label_set_align(label_date_day, LV_LABEL_ALIGN_CENTER);
  lv_label_set_recolor(label_date_day, true);

  short int yy = 42;
  for(short int dd = 0; dd < 4; dd++) {
    InitLines(dd, yy);
  }
  line_points[36][0] = {12, 228};
  line_points[36][1] = {228, 228}; // 100% battery

  for(short int ii = 0; ii < 37; ii++) {
    lines[ii] = lv_line_create(lv_scr_act(), NULL);
    lv_obj_add_style(lines[ii], LV_LINE_PART_MAIN, &style_minute_on);
    lv_line_set_points(lines[ii], line_points[ii], 2);
  }
  lv_obj_add_style(lines[36], LV_LINE_PART_MAIN, &style_battery);

  taskRefresh = lv_task_create(RefreshTaskCallback, LV_DISP_DEF_REFR_PERIOD, LV_TASK_PRIO_MID, this);
  Refresh();
}

WatchFaceBold::~WatchFaceBold() {
  lv_task_del(taskRefresh);

  lv_style_reset(&style_battery);
  lv_style_reset(&style_hour_on);
  lv_style_reset(&style_minute_on);
  lv_style_reset(&style_digit_off);

  lv_obj_clean(lv_scr_act());
}

void WatchFaceBold::InitLines(short int _d, short int _y) {
  // C_SLO = 10; // slope
  // C_GAP = 8; // leading between digits (line thickness encroaches)
  // C_SML = 40; // top cell height
  // C_BIG = 60; // bottom cell height
  // C_WID = 40; // digit width
  // C_PAD = 8; // (240 - ((C_WID + C_SLO) * 4) - (C_GAP * 3)) / 2

  short int offset = _d * 9;
  short int x1 = 17 + (_d * 58); // (C_PAD - 1) + C_SLO + (_d * (C_WID + C_GAP + C_SLO));
  short int y1 = _y;
  short int x2 = x1 - 4; // (C_SLO * (C_SML / (C_SML + C_BIG)));
  short int y2 = y1 + 40; // C_SML
  short int x3 = x1 - 10; // C_SLO
  short int y3 = y2 + 60; // C_BIG
  short int x11 = x1 + 40; // C_WID
  short int x21 = x2 + 40; // C_WID
  short int x31 = x3 + 40; // C_WID

  line_points[offset + 0][0] = {x1, y1};    line_points[offset + 0][1] = {x2, y2};    // vLT
  line_points[offset + 1][0] = {x11, y1};   line_points[offset + 1][1] = {x21, y2};   // vRT
  line_points[offset + 2][0] = {x2, y2};    line_points[offset + 2][1] = {x3, y3};    // vLB
  line_points[offset + 3][0] = {x21, y2};   line_points[offset + 3][1] = {x31, y3};   // vRB
  line_points[offset + 4][0] = {x1, y1};    line_points[offset + 4][1] = {x11, y1};   // hT
  line_points[offset + 5][0] = {x2, y2};    line_points[offset + 5][1] = {x21, y2};   // hM
  line_points[offset + 6][0] = {x3, y3};    line_points[offset + 6][1] = {x31, y3};   // hB
  line_points[offset + 7][0] = {x11, y1};   line_points[offset + 7][1] = {x2, y2};    // dT
  line_points[offset + 8][0] = {x21, y2};   line_points[offset + 8][1] = {x3, y3};    // dB
}

void WatchFaceBold::DrawLines(short int _d, short int _n, lv_style_t *_c) {
  short int offset = _d * 9;
  switch(_n) {
    case 0:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, &style_digit_off);
      break;
    case 1:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, &style_digit_off);
      break;
    case 2:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, _c);
      break;
    case 3:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, &style_digit_off);
      break;
    case 4:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, &style_digit_off);
      break;
    case 5:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, &style_digit_off);
      break;
    case 6:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, &style_digit_off);
      break;
    case 7:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, _c);
      break;
    case 8:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, &style_digit_off);
      break;
    case 9:
      lv_obj_add_style(lines[offset + 0], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 1], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 2], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 3], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 4], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 5], LV_LINE_PART_MAIN, _c);
      lv_obj_add_style(lines[offset + 6], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 7], LV_LINE_PART_MAIN, &style_digit_off);
      lv_obj_add_style(lines[offset + 8], LV_LINE_PART_MAIN, _c);
      break;
  }
}

void WatchFaceBold::Refresh() {
  currentDateTime = dateTimeController.CurrentDateTime();
  if (currentDateTime.IsUpdated()) {
    hour = dateTimeController.Hours();
    minute = dateTimeController.Minutes();
    if ((hour != currentHour) || (minute != currentMinute)) {
      currentHour = hour;
      currentMinute = minute;
      if (hour < 6 || hour > 17) {
        nighttime = true;
        lv_style_set_line_color(&style_hour_on, LV_STATE_DEFAULT, lv_color_hex(0xafafaf));
        lv_style_set_line_color(&style_minute_on, LV_STATE_DEFAULT, LV_COLOR_WHITE);
        lv_obj_set_style_local_bg_color(background, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLACK);
      } else {
        nighttime = false;
        lv_style_set_line_color(&style_hour_on, LV_STATE_DEFAULT, lv_color_hex(0x505050));
        lv_style_set_line_color(&style_minute_on, LV_STATE_DEFAULT, LV_COLOR_BLACK);
        lv_obj_set_style_local_bg_color(background, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_WHITE);
      }

      DrawLines(0, hour / 10, &style_hour_on);
      DrawLines(1, hour % 10, &style_hour_on);
      DrawLines(2, minute / 10, &style_minute_on);
      DrawLines(3, minute % 10, &style_minute_on);

      day = dateTimeController.Day();
      dayOfWeek = dateTimeController.DayOfWeek();
      if ((dayOfWeek != currentDayOfWeek) || (day != currentDay)) {
        currentDayOfWeek = dayOfWeek;
        currentDay = day;
        lv_label_set_text_fmt(label_date_day, "%s %02i", dateTimeController.DayOfWeekShortToString(), day);
        lv_obj_align(label_date_day, lv_scr_act(), LV_ALIGN_CENTER, 0, 66);
      }

      batteryPercentRemaining = batteryController.PercentRemaining();
      float batteryPercent = static_cast<float>(batteryPercentRemaining.Get());
      uint8_t rr = 255;
      uint8_t gg = 255;
      if (batteryPercent > 60.0) {
        rr = static_cast<int>(((100.0 - batteryPercent) / 40.0) * 255.0);
      } else if (batteryPercent < 60.0) {
        gg = static_cast<int>((batteryPercent / 60.0) * 255.0);
      }
      battery_colour = lv_color_make(rr, gg, 0);
      uint8_t bb = static_cast<int>((batteryPercent * 216.0) / 100.0) + 12;
      line_points[36][1] = {bb, 228}; // [12, 228]

      lv_style_set_line_color(&style_battery, LV_STATE_DEFAULT, battery_colour);
      lv_obj_set_style_local_text_color(label_date_day, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, battery_colour);
      lv_line_set_points(lines[36], line_points[36], 2); // need to do this after (re)setting points/style to trigger redraw?
    }
  }

  isCharging = batteryController.IsCharging();
  if (isCharging.IsUpdated()) {
    if (isCharging.Get()) { // override colour, if charging
      lv_style_set_line_color(&style_battery, LV_STATE_DEFAULT, LV_COLOR_BLUE);
      lv_obj_set_style_local_text_color(label_date_day, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLUE);
      lv_line_set_points(lines[36], line_points[36], 2); // need to do this after (re)setting points/style to trigger redraw?
    } else {
      lv_style_set_line_color(&style_battery, LV_STATE_DEFAULT, battery_colour);
      lv_obj_set_style_local_text_color(label_date_day, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, battery_colour);
      lv_line_set_points(lines[36], line_points[36], 2); // need to do this after (re)setting points/style to trigger redraw?
    }
  }
}

