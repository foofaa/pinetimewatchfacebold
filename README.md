A simple, BOLD watchface for PineTime / InfiniTime.

I wrote my own watchface, and replaced the 'PineTimeStyle' watchface with my new one. This repo just has the files that define my new watchface - you'll need to do your own integration into the InfiniTime repo (I used the [Docker version](https://github.com/InfiniTimeOrg/InfiniTime/blob/develop/doc/buildWithDocker.md) of the project to build my watchface).

Whilst you should be able to recreate my watchface using the information in this repo, the purpose of the repo is more to inspire you to build your own thing, and give you a few tips to get yourself started. It should also be noted that I'm not a C++ developer - hopefully you won't find my code too offensive! ;-)

![DAY mode](PT.day.png) - ![NIGHT mode](PT.night.png) - ![Photo](PT.photo.jpg)

## Design Goals ##

* A minimalist watchface - just the time (24hr), day/date, and battery status.
* The largest possible text for the time, with minutes highlighted more than hour.
* Day/night mode (switches at 06h00 and 18h00).
* Colour-coded battery meter.

## Fonts ##
The screenshots, above, do not show the font I used for the day/date text (OCR-A, size 28). Unfortunately I have no authority to distribute the font I used - you'll need to drop your own copy of the font into the project ('src/displayapp/fonts/'), and then declare it inside 'src/libs/lv_conf.h' (add your font below the line "LV_FONT_DECLARE(lv_font_sys_48)").

[More info about font stuff](https://github.com/InfiniTimeOrg/InfiniTime/blob/develop/src/displayapp/fonts/README.md)

## Summary of Changes to InfiniTime Repo ##
New files: src/displayapp/fonts/OCRA.ttf; src/displayapp/screens/WatchFaceBold.cpp; src/displayapp/screens/WatchFaceBold.h

Modified files: src/CMakeLists.txt; src/displayapp/screens/Clock.cpp; src/displayapp/screens/Clock.h; src/displayapp/screens/settings/SettingWatchFace.h; src/libs/lv_conf.h

#### Changes to existing InfiniTime files: ####

In the following files...

* src/CMakeLists.txt
* src/displayapp/screens/Clock.cpp
* src/displayapp/screens/Clock.h
* src/displayapp/screens/settings/SettingWatchFace.h

... replace the string "PineTimeStyle" with "Bold"

In 'src/displayapp/screens/Clock.cpp' remove the "motionController" parameter from the end of the "std::make_unique<Screens::WatchFaceBold>()" function signature.

## InfiniTime 1.10.0
As of this release, font management stuff has been improved - no need to 'compile' your own fonts any more. To include the OCR-A font we now drop the .TTF into the 'src/displayapp/fonts/' folder, and add our font into:

'src/displayapp/fonts/fonts.json':

    "ocra_28": {
        "sources": [
            {
                "file": "OCRA.ttf",
                "range": "0x20-0x7e"
            }
        ],
        "bpp": 1,
        "size": 28
    },

'src/displayapp/fonts/CMakeLists.txt':

    set(FONTS jetbrains_mono_42 ocra_28 jetbrains_mono_76 ...

