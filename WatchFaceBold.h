#pragma once

#include <lvgl/src/lv_core/lv_obj.h>
#include <chrono>
#include <cstdint>
#include <memory>
#include "displayapp/screens/Screen.h"
#include "components/datetime/DateTimeController.h"
#include "components/battery/BatteryController.h"
#include "components/ble/BleController.h"
#include "components/ble/NotificationManager.h"

namespace Pinetime {
  namespace Controllers {
    class Settings;
    class Battery;
    class Ble;
    class NotificationManager;
  }
  namespace Applications {
    namespace Screens {

      class WatchFaceBold : public Screen {
      public:
        WatchFaceBold(DisplayApp* app,
                        Controllers::DateTime& dateTimeController,
                        Controllers::Battery& batteryController,
                        Controllers::Ble& bleController,
                        Controllers::NotificationManager& notificationManager,
                        Controllers::Settings& settingsController);

        ~WatchFaceBold() override;

        void Refresh() override;

      private:
        bool nighttime = false;

        uint8_t currentDay = 0;
        uint8_t currentHour = 0;
        uint8_t currentMinute = 0;
        uint8_t day = 0;
        uint8_t hour = 0;
        uint8_t minute = 0;

        DirtyValue<bool> isCharging {};
        DirtyValue<std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>> currentDateTime;
        DirtyValue<uint8_t> batteryPercentRemaining {0};

        Pinetime::Controllers::DateTime::Days currentDayOfWeek = Pinetime::Controllers::DateTime::Days::Unknown;
        Pinetime::Controllers::DateTime::Days dayOfWeek;

        lv_color_t battery_colour;

        lv_point_t line_points[37][2]; // (4 * 9) + 1 // 4-digits, 9-lines-per-digit, 1-battery-line (2-points-per-line)

        lv_obj_t* lines[37];
        lv_obj_t* background;
        lv_obj_t* label_date_day;

        lv_style_t style_battery;
        lv_style_t style_digit_off;
        lv_style_t style_hour_on;
        lv_style_t style_minute_on;

        Controllers::DateTime& dateTimeController;
        Controllers::Battery& batteryController;
        Controllers::Ble& bleController;
        Controllers::NotificationManager& notificationManager;
        Controllers::Settings& settingsController;

        void InitLines(short int _d, short int _y);
        void DrawLines(short int _d, short int _n, lv_style_t *_c);

        lv_task_t* taskRefresh;
      };
    }
  }
}
